export function fooOne(x) {
  if (x === 1) {
    return x + 1;
  }

  if (x === 2) {
    return x + 1;
  }

  const result = x + 1;

  return result + 1;
}