import { describe, it } from 'node:test';
import assert from 'node:assert';
import { fooOne } from './foo.js';

describe('fooTest', () => {
  it('returns result', () => {
    const result = fooOne(12);

    assert.strictEqual(result, 14);
  });

  it('handles when x equals to 2', () => {
    const result = fooOne(2);

    assert.strictEqual(result, 3);
  });
});
